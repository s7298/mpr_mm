import java.util.Vector;

public class Role {
	private String name;
	private Vector<Permission> permissions;
	
	//region Name getters/setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	//endregion
	
	//region Permissions getters/setters
	public Vector<Permission> getPermissions() {
		return permissions;
	}
	
	public void setPermissions(Vector<Permission> permissions) {
		this.permissions = permissions;
	}
	
	public void addPermission(Permission permission) {
		permissions.addElement(permission);
	}
	
	public void removePermission(Permission permission) {
		permissions.removeElement(permission);
	}
	//endregion
}
