import java.util.Vector;

public class Person {
	private String firstName;
	private String lastName;
	private Vector<Address> addresses;
	private Vector<Role> roles;
	
	//region Person getters/setters
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	//endregion
	
	//region Addresses getters/setters
	public Vector<Address> getAddresses() {
		return addresses;
	}
	
	public void setAddresses(Vector<Address> addresses) {
		this.addresses = addresses;
	}
	
	public void addAddress(Address address) {
		addresses.addElement(address);
	}
	
	public void removeAddress(Address address) {
		addresses.removeElement(address);
	}
	//endregion
	
	//region Roles getters/setters
	public Vector<Role> getRoles() {
		return roles;
	}
	
	public void setRoles(Vector<Role> roles) {
		this.roles = roles;
	}
	
	public void addRole(Role role) {
		roles.addElement(role);
	}
	
	public void removeRole(Role role) {
		roles.removeElement(role);
	}
	//endregion
}
